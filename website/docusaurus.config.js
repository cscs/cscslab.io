/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'cscs',
  tagline: 'common source computer services',
  url: 'https://cscs.gitlab.io',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'cscs', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.
  themeConfig: {
    colorMode: {
      defaultMode: 'dark',
      disableSwitch: false,
      respectPrefersColorScheme: true,
    },
    navbar: {
      logo: {
        alt: 'My Site Logo',
        //src: 'img/logo.svg',
        src: 'img/cscsmono.png',
        srcDark: 'img/cscsmonodrk.png',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {to: 'blog', label: 'Blog', position: 'left'},
        {
          type: 'dropdown',
          label: 'Tools',
          position: 'left',
          items: [
            {
              to: 'dnsdig',
              label: 'dnsdig',
            },
            {
              to: 'maclean',
              label: 'maclean',
            },
            {
              to: 'mapare',
              label: 'mapare',
            },
          ],
        },
        {
          type: 'search',
          position: 'right',
        },
      ],
    },
    footer: {
      links: [
        {
          title: 'Documentation',
          items: [
            {
              label: 'Introduction',
              to: 'docs/',
            },
            {
              label: 'Tools',
              to: 'docs/tools',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Ko-fi',
              href: 'https://ko-fi.com/commonsourcecs',
            },
            {
              label: 'Manjaro Forums',
              href: 'https://forum.manjaro.org',
            },
          ],
        },
        {
          title: 'Development',
          items: [
            {
              label: 'GitHub',
              href: 'https://github.com/commonsourcecs',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/cscs',
            },
          ],
        },
      ],
      copyright: `<br /><a href="https://creativecommons.org/licenses/by-nc/4.0/">CC BY-NC 4.0 ${new Date().getFullYear()}</a>`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/cscs/cscs.gitlab.io/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/cscs/cscs.gitlab.io/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
