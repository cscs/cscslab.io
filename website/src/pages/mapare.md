---
title: mapare
hide_table_of_contents: true
---

<div align="center">

## mapare

A script to automate curating and installing manjaro package lists.

<img src="https://gitlab.com/cscs/cscs.gitlab.io/-/raw/master/website/static/img/mapare_demo.gif" height="500px" width="625px" />   

######   

Introduction and guides at the [Docs Page](/docs/mapare)  

Download and more information at the [Gitlab Page](https://gitlab.com/cscs/mapare)

</div>
