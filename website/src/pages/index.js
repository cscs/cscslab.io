import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: 'Guides',
    imageUrl: 'img/book-open-reader-solid.svg',
    linker: 'docs/',
    description: (
      <>
        A collection of tutorials.
      </>
    ),
  },
  {
    title: 'Tools',
    imageUrl: 'img/screwdriver-wrench-solid.svg',
    linker: 'docs/tools',
    description: (
      <>
        Hopefully helpful utilities. 
      </>
    ),
  },
  {
    title: 'Donate',
    imageUrl: 'img/hand-holding-dollar-solid.svg',
    linker: 'donate/',
    description: (
      <>
        Everything is free. Donations Welcome. 
      </>
    ),
  },
];

function Feature({imageUrl, title, linker, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
    <Link
    to={useBaseUrl(linker)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      </Link>
      <p>{description}</p>
    </div>
  );
}

export default function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`common source computer services`}
      description="Description will go into a meta tag in <head />">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              lore
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}
