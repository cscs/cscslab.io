import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: 'Ko-fi',
    imageUrl: 'img/koficirc.png',
    linker: 'https://ko-fi.com/X8X0VXZU',
  },
  {
    title: 'PayPal',
    imageUrl: 'img/paypalcirc.png',
    linker: 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52',
  },
];

function Feature({imageUrl, title, linker}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col-md-4', styles.feature)}>
    <Link
    to={useBaseUrl(linker)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <div className="text--center">
        <h3>{title}</h3>
      </div>
        </Link>
    </div>
  );
}

export default function Donate() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Donate2`}
      description="Description will go into a meta tag in <head />">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">Everything is free.</h1>
          <p className="hero__subtitle">Donations are welcome. Thank you for the support!</p>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}
