---
title: dnsdig
hide_table_of_contents: true
---

<div align="center">

## dnsdig

A little script using dig or drill to measure response times of DNS resolvers.

<img src="https://gitlab.com/cscs/cscs.gitlab.io/-/raw/master/website/static/img/dnsdig_demo.gif" height="500px" width="625px" />   

######   

Introduction and guides at the [Docs Page](/docs/dnsdig)  

Download and more information at the [Gitlab Page](https://gitlab.com/cscs/dnsdig)
</div>
