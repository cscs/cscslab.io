---
title: Donate3
hide_table_of_contents: true
---

## How to Donate

<div class="row">
<div className="text--center">
  <div class="col-md-8" markdown="1">
  Some text.    
  and more text.    
  and more.
  </div>
  <div class="col-md-4" markdown="1">
  <img height="200px" class="center-block" src="/img/cscsmono.png" />
  and text.   
  and text.   
  </div> 
</div>
</div>


<div style={{ width: "50%" }}>

[<Icon icon="fa-solid fa-mug-hot" size="xl" /> **Ko-fi**](https://ko-fi.com/commonsourcecs)  

</div>

<div style={{ width: "50%" }}>

[<Icon icon="fa-brands fa-paypal" size="xl" /> **PayPal**](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52) 

</div>

<br />
