---
title: maclean
hide_table_of_contents: true
---

<div align="center">

## maclean

A script to automate some relatively safe cleaning activities targeted at Manjaro Linux.

<img src="https://gitlab.com/cscs/cscs.gitlab.io/-/raw/master/website/static/img/maclean_demo.gif" height="500px" width="625px" />   

######   

Introduction and guides at the [Docs Page](/docs/maclean)  

Download and more information at the [Gitlab Page](https://gitlab.com/cscs/maclean)

</div>
