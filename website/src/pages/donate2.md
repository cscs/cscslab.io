---
title: Donate2
---

## How to Donate

<br />

## [<Icon icon="fa-solid fa-mug-hot" size="xl" /> **Ko-fi**](https://ko-fi.com/commonsourcecs)  

<br />

## [<Icon icon="fa-brands fa-paypal" size="xl" /> **PayPal**](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52)  

<br />
