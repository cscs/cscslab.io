module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Tutorials',
      items: [
        'welcome',
        {
          type: 'category',
          label: 'Networking',
          items: [
            'file-sharing-services',
            'pastebin-services',
          ],
        },
        {
          type: 'category',
          label: 'Performance',
          items: [
            'disable-watchdogs',
            'enable-zram',
          ],
        },
        {
          type: 'category',
          label: 'Recovery',
          items: [
            'recovery-boot-procedures',
          ],
        },
      ],
    },
    {
          type: 'category',
          label: 'Tools',
          items: [
            'tools',
            'dnsdig',
            'maclean',
            'mapare',
          ],
    },
  ],
};
