---
title: Recovery Boot Procedures
---

## Introduction

If your desktop does not load properly then you might still be able to use most functionality by following one of the methods below.


## Virtual Console or TTY

TTY refers to teletypewriter, which may seem out of place for a modern machine, but is commonly used today for virtual consoles. 

By default your system will have multiple TTYs, with each corresponding to an Fx key. Your desktop is assigned to one as well - most often F1 or F2.

If there is a problem with the graphical session the virtual console may still be operable and accessible by one of the TTYs.

You can change TTYs through a combination of Ctrl+Alt+Fx (F1-F6). 

Meaning often our first steps would be <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>F3</kbd>.

If greeted with a login prompt then log in with your user name and password. 



## Grub options and Boot Parameters

**If you are never prompted for sign-in you can still take action by changing your boot options.**

At the Grub menu, make your selection and and hit <kbd>E</kbd> to edit the kernel boot line.

>Note: Some systems may have Grub hidden by default.  
>If this is the case tap <kbd>Esc</kbd> or <kbd>Shift</kbd> to make it appear.

### Runlevel 3

Linux systems have defined 'run-level's denoting which services are loaded at boot. 
They are defined as follows:

0  = _halt_, 1 = _single-user-mode_, 2 = _multi-user-mode_, 3 = _multi-user-mode+networking_, 4 = undefined, 5 = _multi-user-mode+networking+displaymanager_, 6 = _reboot_

As shown above, we normally boot into runlevel 5. But it can be useful, such as in the case of broken gfx drivers, to use runlevel 3. 

In the grub editor look for the line beginning with `linux` , it should look like this:

`linux /boot/vmlinuz-4.11-x86_64 root=UUID=0a01099a-1e33-489a-a2de-10104e8492f5 rw quiet`

Simply add a `3` to the options at the end of the line (and optionally remove 'quiet') so it appears like this:

`linux /boot/vmlinuz-4.11-x86_64 root=UUID=0a01099a-1e33-489a-a2de-10104e8492f5 rw 3`

To apply changes and continue booting use the <kbd>F10</kbd> key.

If greeted with a login prompt then log in with your user name and password. 



### Other options

A handful of other options that _may_ be helpful are:  
 `acpi=off`, `nolapic`, `nomodeset`, and %driver%.modeset=0 ex: `radeon.modeset=0`   



## chroot

Just about everything you would need to know is at the archwiki:  
https://wiki.archlinux.org/title/Chroot

### manjaro-chroot

manjaro-chroot is a script that attempts to automate chrooting.

If you are unable to recover using the installed system you can use another medium such as a CD or USB

Create or just reuse an already existing Manjaro Live USB (such as the one you used to install your system).

Once booted in the live system issue the following command in the terminal:

```text
manjaro-chroot -a
```
