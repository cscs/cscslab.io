---
title: Disable Watchdogs
---

## Introduction

There is already a pretty good Arch wiki article:  
https://wiki.archlinux.org/title/Improving_performance#Watchdogs

Which already does a decent job explaining what it is and why we want to disable it:  

> According to [Wikipedia:Watchdog timer](https://en.wikipedia.org/wiki/Watchdog_timer):
> 
> A watchdog timer [...] is an electronic timer that is used to detect and recover from computer malfunctions. During normal operation, the computer regularly resets the watchdog timer [...]. If, [...], the computer fails to reset the watchdog, the timer will elapse and generate a timeout signal [...] used to initiate corrective [...] actions [...] typically include placing the computer system in a safe state and restoring normal system operation.
> 
> Many users need this feature due to their system's mission-critical role (i.e. servers), or because of the lack of power reset (i.e. embedded devices). Thus, this feature is required for a good operation in some situations. On the other hand, __normal users (i.e. desktop and laptop) do not need this feature and can disable it.__


> ... [Disabling it] will speed up your boot and shutdown, because one less module is loaded. Additionally disabling watchdog timers increases performance and [lowers power consumption](https://wiki.archlinux.org/title/Power_management#Disabling_NMI_watchdog).

As well as a few ways to disable the watchdog(s)  

> To disable watchdog timers (both software and hardware), append `nowatchdog` to your boot parameters.
> 
> The `nowatchdog` boot parameter may not work for the Intel TCO hardware watchdog [[1]](https://bbs.archlinux.org/viewtopic.php?id=221239). In this circumstance, the kernel module for the TCO may be disabled using the `modprobe.blacklist=iTCO_wdt` [kernel parameter](https://wiki.archlinux.org/title/Kernel_parameter).
> 
> If you are using AMD Ryzen CPUs, also check `sp5100-tco` in your [journal](https://wiki.archlinux.org/title/Journal). This is the hardware watchdog inside [AMD 700 chipset series](https://en.wikipedia.org/wiki/AMD_700_chipset_series). To disable it:
> 
> /etc/modprobe.d/disable-sp5100-watchdog.conf
> 
> blacklist sp5100_tco
> 
> Or use the `modprobe.blacklist=sp5100_tco` [kernel parameter](https://wiki.archlinux.org/title/Kernel_parameter).


The problem is that this is not at all an exhaustive list of possible watchdog modules.  
One appendix that can be useful is found here:  
https://www.kernel.org/doc/Documentation/watchdog/watchdog-parameters.txt

But even _that_ does not cover all possible watchdog modules. 

So here I will attempt to provide a few steps to find and disable any watchdogs that just "arent like the other modules." 

## Find watchdog modules

You will notice there are some common features of watchdog modules - namely titles including some assortment of "TC", "TCO", "WD", or "WDT", in varying upper and lower cases. 

My suggestion would be to query modules, sorting them by those clues:
```text
lsmod | grep -i 'tc\|wd'
```
Some modules may appear self-explanatory, and others less so. 

See this example:
```text title="$ lsmod | grep 'tc\|wd'"
snd_hwdep              12288  1 snd_hda_codec
snd                   126976  19 snd_hda_codec_generic,snd_seq,snd_seq_device,snd_hda_codec_hdmi,snd_hwdep,snd_hda_intel,snd_hda_codec,snd_hda_codec_realtek,snd_timer,snd_pcm
wdat_wdt               16384  0
```

In this case `wdat_wdt` seems a likely candidate.  
We can use `modinfo` to provide more details.  
```text title="$ modinfo wdat_wdt"
filename:       /lib/modules/6.6.1-linux/kernel/drivers/watchdog/wdat_wdt.ko.xz
alias:          platform:wdat_wdt
license:        GPL v2
description:    ACPI Hardware Watchdog (WDAT) driver
author:         Mika Westerberg <mika.westerberg@linux.intel.com>
srcversion:     2D87893F9044A672B7B6A81
depends:        
retpoline:      Y
intree:         Y
name:           wdat_wdt
vermagic:       6.6.1-linux SMP preempt mod_unload modversions 
parm:           nowayout:Watchdog cannot be stopped once started (default=0) (bool)
parm:           timeout:Watchdog timeout in seconds (default=30) (int)
```

The extra information verifies this module as a watchdog, and thus, fit for blacklisting. 

## Blacklist watchdogs

Once the module is blacklisted it should not be loaded on next boot.

Create `%filename%.conf` at `/etc/modprobe.d/`, appending one line of `blacklist %module%` for each module to be blacklisted.

```text title="/etc/modprobe.d/blacklist-watchdog.conf"
# watchdog
blacklist sp5100_tco
# extra watchdog 
blacklist wdat_wdt
```
