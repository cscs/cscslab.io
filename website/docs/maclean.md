---
title: maclean
---

A script to automate some relatively safe cleaning activities targeted at Manjaro Linux.


### Example Run 

```text title="$ maclean"
Clean thumbnail cache of files not accessed for 2 weeks (y/N)? 
Done

Clean $HOME cache of files not accessed for 2 weeks (y/N)? 
Done

Clean AUR Helper cache of all build files (y/N)? 
Done

Clean all unused repositories and uninstalled packages from cache (y/N)? 
Done
```

### Operations Table


| Operation            | Clean | Remove |                          Paths                         |                                                                                Notes |
|:---------------------|:-----:|:------:|:------------------------------------------------------:|-------------------------------------------------------------------------------------:|
| User-defined Junk    |       |    ✔   |                ~/.adobe , ~/.macromedia                |                      Default targets listed. Edit or add to the array in the script. |
| Thumbnail cache      |   ✔   |        |                   ~/.cache/thumbnails                  |                                          Keep anything accessed within last 2 weeks. |
| $HOME cache          |   ✔   |        |                        ~/.cache                        |                Keep select directories*. Keep anything accessed within last 2 weeks. |
| Journal logs         |   ✔   |        |                                                        |                                          Keep anything accessed within last 2 weeks. |
| SNAP data            |   ✔   |    ✔   |            ~/.snap , ~/snap , /var/lib/snapd           |                                       Cleaning if installed. Removal if uninstalled. |
| Flatpak data         |   ✔   |    ✔   | ~/.var/app , ~/.local/share/flatpak , /var/lib/flatpak |                                       Cleaning if installed. Removal if uninstalled. |
| npm cache            |   ✔   |    ✔   |                         ~/.npm                         |                                       Cleaning if installed. Removal if uninstalled. |
| pip cache            |   ✔   |    ✔   |                      ~/.cache/pip                      |                                       Cleaning if installed. Removal if uninstalled. |
| pkgfile cache        |       |    ✔   |                   /var/cache/pkgfile                   |                                                              Removal if uninstalled. |
| PackageKit cache     |   ✔   |    ✔   |       /var/cache/PackageKit , /var/lib/PackageKit      |                                       Cleaning if installed. Removal if uninstalled. |
| AUR Helper cache     |   ✔   |    ✔   |                                                        |                   Loop through supported AUR Helpers to invoke their cleaning flags. |
| Package cache        |   ✔   |        |                  /var/cache/pacman/pkg                 | Remove all uninstalled cache, unused repos, and/or all but 2 latest installed cache. |
| Orphan Packages      |       |    ✔   |                  /var/cache/pacman/pkg                 |                       Remove all orphan packages. Please review before confirmation. |
| pacnew+pacsave files |       |        |                                                        |                                               Warning Only. Use `pacdiff` to manage. |

### Project Link

Download and more information at the [Gitlab Page](https://gitlab.com/cscs/maclean)

<br />
