---
title: Enable zram
---

## Introduction

From the [Archwiki](https://wiki.archlinux.org/title/Zram)

> zram, formerly called compcache, is a Linux kernel module for creating a compressed block device in RAM, i.e. a RAM disk with on-the-fly disk compression. The block device created with zram can then be used for swap or as a general-purpose RAM disk. The two most common uses for zram are for the storage of temporary files (/tmp) and as a swap device. Initially, zram had only the former function, hence the original name "compcache" ("compressed cache").
> 
> Initially the created zram block device does not reserve or use any RAM. Only as files need or want to be swapped out, they will be compressed and moved into the zram block device. The zram block device will then dynamically grow or shrink as required.
> 
> For example, consider an example system with 32 GiB RAM where zram is configured with 64 GiB capacity. Assuming that zstd employs a 1:4 compression ratio, the actual compressed zram block size on physical RAM when fully occupied will be around 16 GiB. Therefore:
> 
>     RAM and zram completely occupied: 16 GiB RAM + 64 GiB zram (~16 GiB in RAM)
>     Normal usage without swapping: 32 GiB RAM + 0 GiB zram
>     Normal usage with light swapping: 30 GiB RAM + 8 GiB zram (~2 GiB in RAM)
>     Without any zram configuration: 32 GiB RAM
> 
> Therefore, zram always offers the advantage of being able to store more content in RAM.
> Note:
> 
>     If the related zswap kernel feature remains enabled, it will prevent zram from being used effectively. This is because zswap functions as a swap cache in front of zram, intercepting and compressing evicted memory pages before they can reach zram. Despite the output of zramctl(8), most of zswap is unused in this circumstance.
>     As discussed above, when configuring zram, the size of the zram device controls the maximum uncompressed amount of data it can store, NOT the maximum compressed size. You can configure the zram's size to be equal to or even greater than your system's physical RAM capacity, as long as the compressed size on physical RAM will not exceed your system's physical RAM capacity.
>     Hibernating to swap on zram is not supported, even when zram is configured with a backing device on permanent storage. logind will protect against trying to hibernate to a swap space on zram.


## Disable zswap

One way is to add to the boot options `zswap.enabled=0`.

Which can be accomplished by adding it to one of the `CMDLINE` lines in `/etc/default/grub`.

Assuming no other options then the line would appear as:
``` title="/etc/default/grub"
[...]
GRUB_CMDLINE_LINUX="zswap.enabled=0"
[...]
```
To apply the option update grub:
```text
sudo update-grub
```
And reboot.

## Install zram-generator

Using pacman:
```text
sudo pacman -Syu zram-generator
```

### Configure zram-generator

Edit the file `/etc/systemd/zram-generator.conf`

The following basic configuration will create a zram swap device equal to the size of your memory.  

```text title="/etc/systemd/zram-generator.conf"
[zram0]
zram-size = ram / 1
compression-algorithm = zstd
swap-priority = 100
fs-type = swap
```

### Start zram devices

Devices should be automatically enabled on reboot.  

Alternatively, to start them right away check for configuration changes:
```text 
systemctl daemon-reload
```
Then start your configured `systemd-zram-setup@zramN.service` instance(s).
```text
systemctl start systemd-zram-setup@zram0
```

## Verify zram as swap

To print `zram` information use `zramctl`

```text title="$ zramctl"
/dev/zram0 zstd         13.5G  208K   40K  848K      12 [SWAP]
```
To print `SWAP` information use `swapon`

```text title="$ swapon"
NAME       TYPE       SIZE USED PRIO
/dev/zram0 partition 13.5G 444K  100
```
