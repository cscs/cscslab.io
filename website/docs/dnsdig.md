---
title: dnsdig
---

A little script using dig or drill to measure response times of DNS resolvers.  

### Options  

A few extra options are avilable in the form of environment variables.  

`DOMAIN=any.url` Can be used to change the domain used for lookups.   
`TESTDNS=X.Y.Y.Z` Can be used to test a resolver IP not already featured.   
`SKIP=1` Will skip the preliminary message as well as all queries except $CURRENTDNS and $TESTDNS if defined.   


### Featured DNS  

| Provider     | Primary IP   | Secondary IP |
|--------------|--------------|--------------|
| [AdGuard](https://adguard-dns.io/public-dns.html) | 94.140.14.14 | 94.140.15.15 |
| [CleanBrowsing](https://cleanbrowsing.org/filters) | 185.228.168.9 | 185.228.169.9 |
| [Control D](https://controld.com/free-dns) | 76.76.2.2 | 76.76.10.2 |
| [Cloudflare](https://1.1.1.1) | 1.1.1.1 | 1.0.0.1 |
| [Gcore](https://gcore.com/public-dns) | 95.85.95.85 | 2.56.220.2 |
| [Google](https://developers.google.com/speed/public-dns) | 8.8.8.8 | 8.8.4.4 |
| [Neustar](https://www.publicdns.neustar) | 156.154.70.2 | 156.154.71.2 |
| [NextDNS](https://nextdns.io) | 45.90.28.105 | 45.90.30.105 |
| [OpenDNS](https://use.opendns.com) | 208.67.222.222 | 208.67.220.220 |
| [Quad9](https://www.quad9.net) | 9.9.9.9 | 149.112.112.112 |

### Project Link

Download and more information at the [GitLab Page](https://gitlab.com/cscs/dnsdig)  

<br />
