---
title: hidpify
---

A script to help configure hidpi screens under Plasma.

Automates KDE font DPI, SDDM DPI, QT scaling, GTK scaling, Firefox and Thunderbird 'pixels per inch'.

:::warning

Originally written during early releases of KDE 5,   
hidpify may not work as expected on later versions of Plasma.

:::

### Project Link

More information at the [GitLab Page](https://gitlab.com/cscs/hidpify)  

<br />
