---
title: dnsdig
---

A script that assists the user in configuring a selection of parameters with the purpose of increasing system performance.    

### Example Run

```text tiitle="$ /maxperfwiz"
maxperfwiz is a script that assists the user in configuring a selection of 
kernel parameters with the purpose of increasing system performance.

All changes to the configurations are first written to temporary files:
     /tmp/maxperfwiz/99-maxperfwiz.conf
     /tmp/maxperfwiz/66-maxperfwiz.rules

When customizations are complete you will be prompted to apply them. The 
following files will be affected:
     /etc/sysctl.d/99-maxperfwiz.conf
     /etc/udev/rules.d/66-maxperfwiz.rules

To remove these files and undo all changes applied by this wizard run
     ./maxperfwiz --remove

Modify sysctl tweaks (Y/n)? 

Modify udev tweaks (Y/n)? 

..cleaning up by removing /tmp/maxperfwiz
```

### Project Link

More information at the [GitLab Page](https://gitlab.com/cscs/maxperfwiz)  

<br />
