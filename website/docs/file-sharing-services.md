---
title: File Sharing Services
---

## Introduction

Sometimes we might want to easily share some files.

While this can be accomplished directly in communications or manually through certain websites like, there also exist a number of open services that allow you to share directly from the command line by using tools you already have installed.

This is great for many situations, including troubleshooting or just plain fun.   
Here I will share a few examples and how to use them.

## Services and Examples

### [0x0.st](https://0x0.st/)
512  MB Limit
```text
curl -F'file=@yourfile.png' https://0x0.st
```

### [Oshi.at](https://oshi.at/)
5000 MB Limit
```text
curl -T /path/to/file https://oshi.at 
```

### [rustypaste](https://rustypaste.shuttleapp.rs/)
500  MB Limit
```text
curl -F 'file=@example.txt' https://rustypaste.shuttleapp.rs
```

### [paste.c-net.org](http://paste.c-net.org/)
50   MB Limit
```text
curl --upload-file '/tmp/file' 'https://paste.c-net.org/'
```

### [temp.sh](https://temp.sh/)
4000 MB Limit
```text
curl -F "file=@test.txt" https://temp.sh/upload
```

### [transfer.sh](https://transfer.sh/)
10000 MB Limit
```text
curl --upload-file ./hello.txt https://transfer.sh
```

## Aliases and other tricks

### rustypaste paster

To *quasi-install* [rustypaste](https://rustypaste.shuttleapp.rs/) pasting and sharing add the following to your bashrc (zshrc untested):
<details>
<summary> paster .bashrc</summary>

```text
paster()
{
    local url='https://rustypaste.shuttleapp.rs'
    if (( $# )); then
        local file
        for file; do
            curl -F "file=@""$file""" "$url" 
        done
    else
        curl -F 'file=@-' "$url"
    fi
}
```
</details>

Then you can use `paster`:

`<command> | paster` to share output.

`paster <filepath> <filepath2>` to share file(s). 

### cnet pastenet

To *quasi-install* [paste.c-net.org ](http://paste.c-net.org) pasting and sharing add the following to your bashrc:
<details>
<summary> paste.cnet.org .bashrc</summary>

```text
pastenet()
{
    local url='https://paste.c-net.org/'
    if (( $# )); then
        local file
        for file; do
            curl -s \
                --data-binary @"$file" \
                --header "X-FileName: ${file##*/}" \
                "$url"
        done
    else
        curl -s --data-binary @- "$url"
    fi
}
pasteget()
{
    local url='https://paste.c-net.org/'
    if (( $# )); then
        local arg
        for arg; do
            curl -s "${url}${arg##*/}"
        done
    else
        local arg
        while read -r arg; do
            curl -s "${url}${arg##*/}"
        done
    fi
}
```
</details>

Then you can use `pastenet` and `pasteget` a few ways:

`<command> | pastenet` to share output.

`pastenet <filepath> <filepath2>` to share file(s).

`pasteget https://paste.c-net.org/<exampleurl>` to download paste.c-net shared content.  

### transfer.sh transfer

To *quasi-install* [transfer.sh](http://paste.c-net.org) sharing add the following to your bashrc:
<details>
<summary> transfer.sh .bashrc</summary>

```text
transfer(){ if [ $# -eq 0 ];then echo "No arguments specified.\nUsage:\n  transfer <file|directory>\n  ... | transfer <file_name>">&2;return 1;fi;if tty -s;then file="$1";file_name=$(basename "$file");if [ ! -e "$file" ];then echo "$file: No such file or directory">&2;return 1;fi;if [ -d "$file" ];then file_name="$file_name.zip" ,;(cd "$file"&&zip -r -q - .)|curl --progress-bar --upload-file "-" "https://transfer.sh/$file_name"|tee /dev/null,;else cat "$file"|curl --progress-bar --upload-file "-" "https://transfer.sh/$file_name"|tee /dev/null;fi;else file_name=$1;curl --progress-bar --upload-file "-" "https://transfer.sh/$file_name"|tee /dev/null;fi;}
```
</details>

Then you can use `transfer`:

`transfer <filepath>` to share a file.

## Notes and Warnings

* There is more detailed info at each host, including options like setting expiration time and more.
* Please [dont abuse](https://github.com/ptpb/pb/issues/246) these services. They are made freely available, so dont knock a good thing.
* Further stipulations, user and privacy clauses and the like can be found at each project host.
* Remember to avoid sharing personal information. 
