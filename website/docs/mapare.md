---
title: mapare
---

A script to automate curating and installing manjaro package lists.

### Example Run

```text title="$ mapare -h"
Manjaro Package Restore

 Retrieve and reinstall package lists

 Options:

  -h to show this help

  -I to install missing packages (--needed by default)

 Install options (require -I):

  -A to install the entire package list
  -D to limit package list to desktop profile
  -E to mark packages as explicitly installed
  -P to only print the package list
  -R to reinstall system packages

  Use the additional --overwrite flag to overwrite existing files.
  (Use With Caution)

```

### Project Link

Download and more information at the [GitLab Page](https://gitlab.com/cscs/mapare)

<br />
